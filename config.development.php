<?php

// ** MySQL settings ** //
/** The name of the database for WordPress */
define('DB_NAME', 'xxxx');

/** MySQL database username */
define('DB_USER', 'xxxx');

/** MySQL database password */
define('DB_PASSWORD', 'xxxx');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

$table_prefix = 'wp_';

?>