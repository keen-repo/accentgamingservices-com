<?php

define('AUTH_KEY',         ';<-]F]k-0<ZBY8!KC`<wV/;HdTUW|M41r(s8F)(Q^~NYRBFpRPh&c%):Ns(uz>Sg');
define('SECURE_AUTH_KEY',  'v/P#.#sH5267!4=@VFz8jjxQJ`s+4EFd,wgRceCqg{_QO6 ^*S*h{.%`SB!>3eIQ');
define('LOGGED_IN_KEY',    '|Hsg*3L~>4%(3d$h-_k;@<)fJ^qfB:6<KvZ;C@}Hd,;L)I~JB0K-$?MBd3buD+Ea');
define('NONCE_KEY',        'eg$|:q~p=}hwQm7k$$;^t24>NdVJT)XYlw5&{o%#roqexU%ISEu`uL{CZRH56%k4');
define('AUTH_SALT',        '~r[}7fBQom!CaXNYl+X}|4>F)KzHzOu*<{?CxFN-{N7H8#XEV(lgfDq-]d~#ZH7.');
define('SECURE_AUTH_SALT', '#U *4PF=jz[ip7UZ-qj_wSOc|TEwJ%~.z[8C:p~(bis?[sY$Ap<Z?8Y_}^nFZ}+/');
define('LOGGED_IN_SALT',   'L<[|UZOm-P`5|;.{[k<4:N-X/<+0|>6OD~<ZZ))I|-IiYtF0(J|e2(-]<93:Ee)T');
define('NONCE_SALT',       'gxA%PVj7x(s:,^E*t7^UD~!AO/>3+h@`p;%$Gze4}()rbd w&X90G-V@@4RMH7zh');





/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');


if (!isset($_SERVER['SERVER_ADDR'])) {
	define('KEEN_HOST', gethostbyname(gethostname()));
} else {
	define('KEEN_HOST', $_SERVER['SERVER_ADDR']);
}


/* Keen Custom Configuration Setup */
if (in_array(KEEN_HOST, array('127.0.0.1', '::1'))) {
	define('KEEN_CONFIG_FILE', 'development');
	define('WP_HOME', 'http://localhost/accentgamingservices.com');
	define('WP_SITEURL', 'http://localhost/accentgamingservices.com');
} else if (KEEN_HOST == '192.168.1.51') {
	define('KEEN_CONFIG_FILE', 'staging');
	define('WP_HOME', 'http://demo.keen.com.mt/projects/public/accentgamingservices.com');
	define('WP_SITEURL', 'http://demo.keen.com.mt/projects/public/accentgamingservices.com');
} else {
	define('KEEN_CONFIG_FILE', 'production');
}

require_once(ABSPATH . 'config.' . KEEN_CONFIG_FILE . '.php');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');